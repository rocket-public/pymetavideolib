from pprint import PrettyPrinter
import requests
from youtube_transcript_api import YouTubeTranscriptApi
from apiclient.discovery import build

# The API endpoint
#url = "https://yt.lemnoslife.com/videos?part=chapters&id="
url = "https://yt.lemnoslife.com/videos?part=chapters,status,contentDetails,music,short,musics,isPaidPromotion,isPremium,isMemberOnly,mostReplayed,qualities,isOriginal,isRestricted&id="
pp = PrettyPrinter()

class YouTube_handler():

    def __init__(self, dev_key):
        self.youtube = build('youtube','v3',developerKey = dev_key)
        # print(type(self.youtube))
        
    def get_videos(self, keyword, part='snippet', type='video', caption='any', embeddable='any', maxResults=10):
        request = self.youtube.search().list(q=keyword, part=part, type=type, 
                                             videoCaption=caption, videoEmbeddable=embeddable,
                                             maxResults=maxResults)
        # print(type(request))
        res = request.execute()
        #from pprint import PrettyPrinter
        #pp = PrettyPrinter()
        #pp.pprint(res)

        ids = []
        for item in res['items']:
            #print(item['snippet']['title'], item['id']['videoId'])
            ids.append((item['snippet']['title'], item['id']['videoId']))
            # pp.pprint(item)

        # sub_request = youtube.videos().list(part='snippet,localizations', id=ids[0])     
        # sres = sub_request.execute()

        # from pprint import PrettyPrinter
        # pp = PrettyPrinter()
        # pp.pprint(sres)

        # print(sres.items[0].snippet.description)
        
        results_list = []

        for id in ids:
            response = requests.get(url + id[1])
            response_json = response.json()
            # print(response_json)
            # print(response_json['chapters'])
            response_list_dict = (response_json['items'])[0]
            response_dict = response_list_dict['chapters']['chapters']

            if len(response_dict) == 0:
                face = ''
            else:
                face = ':-)  ' + str(len(response_dict))
                
            #print(response_json)
            #print(id[0], id[1], face, '----------------------------------')

            caption_request = self.youtube.captions().list(part='snippet', videoId=id[1]).execute()
            #pp.pprint(caption_request)
            
            if len(caption_request['items']) > 0:
                #print('CAPTION --------------------------------------------------------------')
                try:
                    srt = YouTubeTranscriptApi.get_transcript(id[1], languages=['en', 'en-US'])
                    #print(srt)
                    response_json['caption'] = srt
                except:
                    #print('Sorry, no caption')
                    response_json['caption'] = 'None'
                
            #print('VIDEO DETAILS --------------------------------------------------------')        
            video_request = self.youtube.videos().list(part='snippet,contentDetails,statistics,localizations,player,recordingDetails,status,topicDetails', id=id[1]).execute()
            #pp.pprint(video_request)
            response_json['video details info'] = video_request
            
            #print('VIDEO CATEGORY INFO --------------------------------------------------')           
            category_request = self.youtube.videoCategories().list(part = 'snippet', id=((video_request['items'])[0])['snippet']['categoryId']).execute()
            #pp.pprint(category_request) 
            response_json['category info'] = category_request  
            
            #print('CHANNEL INFO ---------------------------------------------------------')
            channel_info_request = self.youtube.channels().list(part = 'brandingSettings,statistics,status,snippet,topicDetails,contentDetails,localizations', id=((video_request['items'])[0])['snippet']['channelId']).execute()
            #pp.pprint(channel_info_request)   
            response_json['channel info'] = channel_info_request 
            
            #print('----------------------------------------------------------------------\n\n')
            
            results_list.append(response_json)
            
        return results_list